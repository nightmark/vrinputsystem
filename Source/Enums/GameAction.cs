﻿namespace LycanByte.InputManager
{
    public enum GameAction
    {
        Left,
        Right,
        LFire,
        RFire,
        LFireAxis,
        RFireAxis,
        LGrip,
        RGrip,
        LHapticButton,
        RHapticButton,
        Menu,
        Pause
    }
}
