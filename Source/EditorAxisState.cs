﻿using UnityEngine;

namespace LycanByte.InputManager {

    public class EditorAxisState : IAxisState {

        private InputManager manager;

        public EditorAxisState(GameAction action, bool isPressed, float offValue, float value)
        {
            Action = action;
            IsPressed = isPressed;
            OffValue = offValue;
            Value = value;
        }
        public GameAction Action { get; set; }

        public bool IsPressed { get; set; }

        public float OffValue { get; set; }

        public float Value { get; set; }

        public void SetManager(InputManager manager)
        {
            this.manager = manager;
        }
    }
}
