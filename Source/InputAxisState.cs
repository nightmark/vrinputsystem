﻿using UnityEngine;

namespace LycanByte.InputManager
{
    [System.Serializable]
    public class InputAxisState : IAxisState 
    {
        public string axisName ;
        [SerializeField]
        private float offValue;
        public float OffValue { get { return offValue; } set { offValue = value; } }
        [SerializeField]
        private GameAction action;
        public GameAction Action { get { return action; } set { action = value; } }
        public Condition condition ;
        private InputManager manager;

        public bool IsPressed
        {
            get
            {
                var val = Input.GetAxis(axisName);

                switch (condition)
                {
                    case Condition.GreaterThan:
                        return val > offValue;
                    case Condition.LessThan:
                        return val < offValue;
                }
                return false;
            }
        }

        public float Value
        {
            get
            {
                var val = Input.GetAxis(axisName);

                switch (condition)
                {
                    case Condition.GreaterThan:
                        if(val > offValue)
                        {
                            return val;
                        }
                        break;
                    case Condition.LessThan:
                        if(val < offValue)
                        {
                            return val;
                        }
                        break;
                }
                return offValue;
            }
        }

        public void SetManager(InputManager manager)
        {
            this.manager = manager;
        }
    }
}
