﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace LycanByte.InputManager
{
    //TODO:Change into struct
    public class ActionState
    {
        public bool isPressed;
        public bool wasJustReleased = false;
        public float value;
        public float holdTime = 0;
    }

    public class InputState : MonoBehaviour
    {
        private Dictionary<GameAction, ActionState> actionStates = new Dictionary<GameAction, ActionState>();
        public bool debugOn = false;

        public void SetButtonValue(GameAction action, bool isPressed, float value)
        {
            if (!actionStates.ContainsKey(action))
                actionStates.Add(action, new ActionState());

            var state = actionStates[action];

            state.wasJustReleased = false;

            if (state.isPressed)
            {
                if (isPressed)
                {
                    state.holdTime += Time.deltaTime;

                    if(debugOn)
                        Debug.Log("Button " + action + " down " + state.holdTime + " value " + value);
                }
                else
                {
                    if (debugOn)
                        Debug.Log("Button " + action + " released " + state.holdTime);
                    state.holdTime = 0;
                    state.wasJustReleased = true;
                }
            }

            state.isPressed = isPressed;
            state.value = value;
        }

        public bool GetActionPressed(GameAction action)
        {
            if (actionStates.ContainsKey(action))
                return actionStates[action].isPressed;
            else
                return false;
        }

        public ActionState GetActionState(GameAction action)
        {
            if (actionStates.ContainsKey(action))
                return actionStates[action];
            else
                return null;
        }
    }
}