﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace LycanByte.InputManager
{
    public class InputManager : MonoBehaviour
    {
        public VRInputAxisState[] inputsVR;
        public InputAxisState[] inputs;
        [Header("Tracked Objects (Right first)")]
        public GenericController[] trackedGenericObjects;
        private SteamVR_TrackedObject[] trackedObjects = new SteamVR_TrackedObject[2];
        public InputState inputState;
        public List<IAxisState> interfaceInputs;

        private Dictionary<GameAction, List<IAxisState>> actionInputs = new Dictionary<GameAction, List<IAxisState>>();
        private const float PULSE_DURATION = 0.075f;

        void Start()
        {
            for (int i = 0; i < trackedGenericObjects.Length; i++)
            {
                var trackedObject = trackedGenericObjects[i].targetController.GetComponent<SteamVR_TrackedObject>();
                trackedObjects[i] = trackedObject;
            }

            RepopulateActionInput();
            foreach (var input in inputs)
                input.SetManager(this);
            foreach (var input in inputsVR)
                input.SetManager(this);

            foreach (var inputList in actionInputs)
                ProcessInput(inputList.Value);
        }

        // Update is called once per frame
        void Update()
        {
            UpdateInputState();
        }

        public void UpdateInputState()
        {
            // In case of multiple inputs for a single action set value to the one that is further from the off zone
            foreach (var inputList in actionInputs)
                ProcessInput(inputList.Value);
        }

        public void AddActionInput(GameAction action, IAxisState inputAxis)
        {
            List<IAxisState> input;
            if (actionInputs.TryGetValue(action, out input))
                input.Add(inputAxis);
            else
                input = new List<IAxisState> { inputAxis };
        }

        public void RepopulateActionInput()
        {
            Debug.Log("Repopulating...");

            var allInputs = inputs.Concat<IAxisState>(inputsVR).ToList();
            foreach (var input in allInputs)
            {
                List<IAxisState> inputList;
                actionInputs.TryGetValue(input.Action, out inputList);
                if (inputList == null)
                {
                    var newList = new List<IAxisState>();
                    newList.Add(input);
                    actionInputs.Add(input.Action, newList);
                }
                else
                {
                    if (!inputList.Contains(input))
                        inputList.Add(input);
                }
            }
        }

        public void TriggerHaptic(ControllerInstance controllerInstance, float duration, ushort power)
        {
            StartCoroutine(HapticCoroutine(controllerInstance, duration, power, PULSE_DURATION));
        }

        private IEnumerator HapticCoroutine(ControllerInstance controllerInstance, float duration, ushort power, float pulseDuration)
        {
            float passedTime = 0;
            var device = GetControllerDevice(controllerInstance);
            float lastTimeInterval = Time.time;

            while (passedTime < duration)
            {
                if (device != null)
                    device.TriggerHapticPulse(power);
                yield return new WaitForSeconds(pulseDuration);
                passedTime += Time.time - lastTimeInterval;
                lastTimeInterval = Time.time;
            }
        }

        private void ProcessInput(List<IAxisState> inputs)
        {
            int inputIndex = 0;
            float maxValue = inputs[0].Value;
            for (int i = 1; i < inputs.Count; i++)
            {
                var input = inputs[i];
                float value = input.Value;

                if (Mathf.Abs(value - input.OffValue) > Mathf.Abs(maxValue - input.OffValue))
                {
                    inputIndex = i;
                    maxValue = value;
                }
            }

            inputState.SetButtonValue(inputs[inputIndex].Action, inputs[inputIndex].IsPressed, inputs[inputIndex].Value);
        }

        void OnValidate()
        {
            RepopulateActionInput();
        }

        public SteamVR_Controller.Device GetControllerDevice(ControllerInstance controllerInstance)
        {
            SteamVR_Controller.Device device = null;
            int controllerIndex = controllerInstance == ControllerInstance.RIGHT ? 0 : 1;

            var trackedObject = trackedObjects[controllerIndex];

            if (trackedObject.index != SteamVR_TrackedObject.EIndex.None)
                device = SteamVR_Controller.Input((int)trackedObject.index);
            return device;
        }
    }

}