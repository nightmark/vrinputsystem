﻿using UnityEngine;
using System;

namespace LycanByte.InputManager
{
    public class EditorInputSimulator : MonoBehaviour
    {
        [SerializeField]
        public EditorAxisState[] axisStates = new EditorAxisState[Enum.GetValues(typeof(GameAction)).Length];
        [SerializeField]
        public bool[] keepPressedStates = new bool[Enum.GetValues(typeof(GameAction)).Length];
        [SerializeField]
        public bool[] justPressedStates = new bool[Enum.GetValues(typeof(GameAction)).Length];
        [SerializeField]
        public float[] actionValues = new float[Enum.GetValues(typeof(GameAction)).Length];

        public InputManager InputManager;

        private bool isInitialized = false;
        public bool IsInitialized
        {
            get { return isInitialized; }
        }

        private void Awake()
        {
            if (!IsInitialized)
                Initialize();
        }

        public void Initialize()
        {
            for (int i = 0; i < actionValues.Length; i++)
            {
                actionValues[i] = 1;
                axisStates[i] = new EditorAxisState((GameAction)i, false, 0, 0);
            }
            isInitialized = true;
        }

        private void Start()
        {
            for (int i = 0; i < actionValues.Length; i++)
            {
                axisStates[i].SetManager(InputManager);
                InputManager.AddActionInput((GameAction)i, axisStates[i]);
            }
        }

        private void Update()
        {
            UpdateInputs();
        }

        private void UpdateInputs()
        {
            foreach (var action in Enum.GetValues(typeof(GameAction)))
            {
                if (keepPressedStates[(int)action] || justPressedStates[(int)action])
                {
                    SetActionState((GameAction)action, true, actionValues[(int)action]);
                    justPressedStates[(int)action] = false;
                }
                else
                    SetActionState((GameAction)action, false);
            }
        }

        public void HoldActionDown(GameAction action, bool isPressed)
        {
            keepPressedStates[(int)action] = isPressed;
        }

        public void HoldActionDown(GameAction action, bool isPressed, float value)
        {
            keepPressedStates[(int)action] = isPressed;
            actionValues[(int)action] = value;
        }

        public void PressActionDown(GameAction action, bool isPressed)
        {
            justPressedStates[(int)action] = isPressed;
        }

        public void PressActionDown(GameAction action, bool isPressed, float value)
        {
            justPressedStates[(int)action] = isPressed;
            actionValues[(int)action] = value;
        }

        private void SetActionState(GameAction action, bool isPressed, float value = 0)
        {
            axisStates[(int)action].IsPressed = isPressed;
            if(isPressed)
                axisStates[(int)action].Value = value;
            else
                axisStates[(int)action].Value = axisStates[(int)action].OffValue;
        }

        public void ClearInputs()
        {
            foreach(var value in Enum.GetValues(typeof(GameAction)))
            {
                keepPressedStates[(int)value] = false;
                justPressedStates[(int)value] = false;
                actionValues[(int)value] = 1f;
            }
            UpdateInputs();
            InputManager.UpdateInputState();
        }
    }
}