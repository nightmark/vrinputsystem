﻿using UnityEngine;

namespace LycanByte.InputManager
{
    public class GenericController : MonoBehaviour
    {
        public GameObject targetController;

        private void Start()
        {
            transform.SetParent(targetController.transform, false);
        }

        public void SetController(GameObject controller)
        {
            targetController = controller;
        }
    }
}
