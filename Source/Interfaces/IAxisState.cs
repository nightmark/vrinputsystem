﻿namespace LycanByte.InputManager 
{
    public interface IAxisState
    {
        GameAction Action { get; set; }

        bool IsPressed { get; }

        float Value { get; }

        float OffValue { get; }

        void SetManager(InputManager manager);
    }
}
