﻿using UnityEngine;

namespace LycanByte.InputManager 
{
    public enum ControllerInstance
    {
        RIGHT = 0,
        LEFT = 1,
        EITHER = 2
    }
    public enum VRControls
    {
        Trigger = 0,
        HairTrigger = 1,
        Touchpad_Horizontal = 2,
        Touchpad_Vertical = 3,
        Touchpad_Button = 4,
        Grip = 5,
        Menu = 6
    }

    [System.Serializable]
    public class VRInputAxisState : IAxisState
    {
        public VRControls Axis;

        [SerializeField]
        private GameAction action;
        public GameAction Action { get { return action; } set { action = value; } }

        public Condition Condition;

        [SerializeField]
        private float offValue;
        public float OffValue { get { return offValue; } set { offValue = value; } }
        
        public ControllerInstance ControllerInstance;

        private InputManager manager;
        private bool isButton = false;

        public void SetManager(InputManager manager)
        {
            if(manager != null)
                this.manager = manager;
        }

        public bool IsPressed {
            get {
                var val = GetAxisValue(Axis);

                switch (Condition)
                {
                    case Condition.GreaterThan:
                        return val > offValue;
                    case Condition.LessThan:
                        return val < offValue;
                }
                return false;
            }
        }

        public float Value
        {
            get
            {
                var val = GetAxisValue(Axis);

                switch (Condition)
                {
                    case Condition.GreaterThan:
                        if(val > offValue)
                        {
                            return val;
                        }
                        break;
                    case Condition.LessThan:
                        if(val < offValue)
                        {
                            return val;
                        }
                        break;
                }
                return offValue;
            }
        }

        float GetAxisValue(VRControls axis)
        {
            Valve.VR.EVRButtonId axisId;
            bool isAxisHorizontal = true;
            switch (axis)
            {
                case VRControls.HairTrigger:
                    axisId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
                    isAxisHorizontal = true;
                    isButton = false;
                    break;
                case VRControls.Trigger:
                    axisId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;
                    isButton = true;
                    break;
                case VRControls.Touchpad_Horizontal:
                    axisId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
                    isAxisHorizontal = true;
                    isButton = false;
                    break;
                case VRControls.Touchpad_Vertical:
                    axisId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
                    isAxisHorizontal = false;
                    isButton = false;
                    break;
                case VRControls.Touchpad_Button:
                    axisId = Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad;
                    isButton = true;
                    break;
                case VRControls.Grip:
                    axisId = Valve.VR.EVRButtonId.k_EButton_Grip;
                    isButton = true;
                    break;
                case VRControls.Menu:
                    axisId = Valve.VR.EVRButtonId.k_EButton_ApplicationMenu;
                    isButton = true;
                    break;
                default:
                    return offValue;
            }
            if (ControllerInstance != ControllerInstance.EITHER)
            {
                return GetAxisValue(axisId, ControllerInstance, isAxisHorizontal);
            }else
            {
                var axisA = GetAxisValue(axisId, ControllerInstance.RIGHT, isAxisHorizontal);
                var axisB = GetAxisValue(axisId, ControllerInstance.LEFT, isAxisHorizontal);

                return axisA > axisB ? axisA : axisB;
            }

        }

        float GetAxisValue(Valve.VR.EVRButtonId axis, ControllerInstance controllerInstance, bool isHorizontalAxis)
        {
            var device = manager.GetControllerDevice(controllerInstance);

            if (device == null)
                return offValue;

            if (!device.connected)
                return offValue;

            if (isButton)
            {
                return ConvertButtonToAxis(axis, device);
            }
            else
            {
                var axisValue = device.GetAxis(axis);

                if (isHorizontalAxis)
                    return axisValue.x;
                else
                    return axisValue.y;
            }
        }

        float ConvertButtonToAxis(Valve.VR.EVRButtonId buttonId, SteamVR_Controller.Device controller)
        {
            return controller.GetPress(buttonId) == true ? 1 : 0;
        }
    }
}
