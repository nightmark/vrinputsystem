﻿using System;
using UnityEditor;
using UnityEngine;

namespace LycanByte.InputManager
{
    [CustomEditor(typeof(EditorInputSimulator))]
    public class EditorInputSimulatorEditor : Editor {
        public override void OnInspectorGUI()
        {
            var currentScript = (EditorInputSimulator)target;

            if (!currentScript.IsInitialized)
                currentScript.Initialize();

            currentScript.InputManager = (InputManager)EditorGUILayout.ObjectField("Input Manager", currentScript.InputManager, typeof(InputManager), true);

            foreach (var action in Enum.GetValues(typeof(GameAction)))
            {
                GUILayout.BeginHorizontal();

                currentScript.keepPressedStates[(int)action] = GUILayout.Toggle(currentScript.keepPressedStates[(int)action], action.ToString());
                currentScript.justPressedStates[(int)action] = GUILayout.Button(action.ToString());
                currentScript.actionValues[(int)action] = EditorGUILayout.FloatField(currentScript.actionValues[(int)action]);

                GUILayout.EndHorizontal();
            }
        }
    }
}
