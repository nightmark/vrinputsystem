Simple Input manager with specific support for HTC vive

# Usage
To use this input manager in your Unity project follow these instructions:

1. Clone these files somewhere in your Unity project. 

2. Place the InputManager component on an object somewhere in your scene and configure the inputs you'd like to use.

3. Place an InputState component on an object somewhere in your scene.

4. During play mode the input state component will have the current values set for each of your game actions so you can easily see if they were pressed or their values.